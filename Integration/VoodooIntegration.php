<?php

namespace MauticPlugin\MauticVoodooBundle\Integration;

use Mautic\PluginBundle\Entity\Integration;
use Mautic\PluginBundle\Integration\AbstractIntegration;
use Mautic\PluginBundle\Helper\oAuthHelper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
/**
 * Class PlivoIntegration.
 */
class VoodooIntegration extends AbstractIntegration
{
    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName()
    {
        return 'Voodoo';
    }

    public function getAuthenticationType()
    {
        return 'none';
    }

    public function getIcon()
    {
        return 'plugins/MauticVoodooBundle/Assets/img/voodoo.png';
    }

    public function getSecretKeys()
    {
        return ['apikey'];
    }

    public function getRequiredKeyFields()
    {
        return [
            'apikey'  => 'mautic.integration.voodoo.apikey',
        ];
    }

    public function appendToForm(&$builder, $data, $formArea)
    {
        if ($formArea == 'keys') {


        }

        if ('features' == $formArea) {
            $builder->add(
                'sending_phone_number',
                TextType::class,
                [
                    'label'      => 'mautic.sms.config.form.sms.sending_phone_number',
                    'label_attr' => ['class' => 'control-label'],
                    'required'   => false,
                    'attr'       => [
                        'class'   => 'form-control',
                        'tooltip' => 'mautic.sms.config.form.sms.sending_phone_number.tooltip',
                    ],
                ]
            );
        }
    }

    public function getFormSettings()
    {
        return [
            'requires_callback'      => false,
            'requires_authorization' => false,
        ];
    }

}