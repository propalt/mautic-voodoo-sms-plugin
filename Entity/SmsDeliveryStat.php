<?php
/*
 * @copyright   2016 Mautic, Inc. All rights reserved
 * @author      Mautic, Inc
 *
 * @link        https://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\MauticVoodooBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\LeadBundle\Entity\Lead;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="MauticPlugin\MauticVoodooBundle\Entity\SmsDeliveryStatRepository")
 */
class SmsDeliveryStat
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var \Mautic\LeadBundle\Entity\Lead
     */
    private $lead;

    /**
     * @var \DateTime
     */
    private $dateSent;

    /**
     * @var string
     */
    public $trackingHash;

    /**
     * @var bool
     */
    private $isFailed;

    /**
     * @var string
     */
    public $reason;

    /**
     * @var \DateTime
     */
    private $dateDelivered;

    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);

        $builder->setTable('sms_message_delivery_stat')
            ->setCustomRepositoryClass(SmsDeliveryStatRepository::class)
            ->addIndex(['tracking_hash'], 'stat_sms_hash_search');

        $builder->addBigIntIdField();
        $builder->addLead(true, 'SET NULL');

        $builder->createField('dateSent', 'datetime')
            ->columnName('date_sent')
            ->build();

        $builder->createField('trackingHash', 'string')
            ->columnName('tracking_hash')
            ->build();

        $builder->createField('isFailed', 'boolean')
            ->columnName('is_failed')
            ->nullable()
            ->build();


        $builder->createField('reason', 'string')
            ->nullable()
            ->build();

        $builder->createField('dateDelivered', 'datetime')
            ->nullable()
            ->columnName('date_delivered')
            ->build();

    }
    

    public function setLead(Lead $lead)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @param string $trackingHash
     *
     * @return SmsDeliveryStat
     */
    public function setTrackingHash($trackingHash)
    {
        $this->trackingHash = $trackingHash;

        return $this;
    }

     /**
     * @param string $isFailed
     *
     * @return SmsDeliveryStat
     */
    public function setIsFailed($isFailed)
    {
        $this->isFailed = $isFailed;

        return $this;
    }

    /**
     * @param \DateTime $dateSent
     *
     * @return Stat
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;

        return $this;
    }

     /**
     * @param string $reason
     *
     * @return SmsDeliveryStat
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }
    
  /**
     * @param \DateTime $date
     *
     * @return SmsDeliveryStat
     */
    public function setDateDelivered($date)
    {
    
        $this->dateDelivered = $date;

        return $this;
    }


}