<?php 

namespace MauticPlugin\MauticVoodooBundle\Model;

use Mautic\CoreBundle\Model\FormModel;
use Mautic\CoreBundle\Helper\ArrayHelper;
use Mautic\CoreBundle\Helper\Chart\LineChart;
use Mautic\CoreBundle\Helper\Chart\ChartQuery;
use Mautic\CoreBundle\Doctrine\Provider\GeneratedColumnsProviderInterface;

class SmsDeliveryStatModel extends FormModel
{
    protected $generatedColumnsProvider;

    public function __construct(GeneratedColumnsProviderInterface $generatedColumnsProvider) {
        $this->generatedColumnsProvider = $generatedColumnsProvider;
    }
    public function getSmsLineChartData($unit, \DateTime $dateFrom, \DateTime $dateTo, $dateFormat = null)
    {
        $chart      = new LineChart($unit, $dateFrom, $dateTo, $dateFormat);
        $query      = new ChartQuery($this->em->getConnection(), $dateFrom, $dateTo, $unit);
       
        // $query->setGeneratedColumnProvider($this->generatedColumnsProvider);

        $q = $query->prepareTimeDataQuery('sms_message_delivery_stat', 'date_sent');
        $sent = $query->loadAndBuildTimeData($q);
        $chart->setDataset($this->translator->trans('mautic.sms.sent.smses'), $sent);
        
        $q = $query->prepareTimeDataQuery('sms_message_delivery_stat', 'date_delivered');
        $sentSuccess = $query->loadAndBuildTimeData($q);
        $chart->setDataset($this->translator->trans('mautic.sms.read.smses'), $sentSuccess);
    
        $q = $query->prepareTimeDataQuery('sms_message_delivery_stat', 'date_sent');
        $q->andWhere($q->expr()->eq('t.is_failed', ':true'))
            ->setParameter('true', true, 'boolean');
        $failed = $query->loadAndBuildTimeData($q);
        $chart->setDataset($this->translator->trans('mautic.sms.failed.smses'), $failed);
        
        return $chart->render();
    }

}