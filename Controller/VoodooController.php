<?php

namespace MauticPlugin\MauticVoodooBundle\Controller;

use Mautic\CoreBundle\Controller\CommonController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use MauticPlugin\MauticVoodooBundle\Entity\SmsDeliveryStat;

class VoodooController extends CommonController
{

    public function indexAction(Request $request) 
    {

        $em = $this->getDoctrine()->getManager();
        $message  = $em->getRepository(SmsDeliveryStat::class)->findOneBy(['trackingHash' => $request->get('message_id')]);
        
        if ($request->get('status') !== 'DELIVERED') {
            $message->setIsFailed(1);
            $message->setReason($request->get('status'));
        }

        $message->setDateDelivered($request->get('delivered_at'));

        $em->flush($message);
           

        return new Response(
            'ok',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }  
}