<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace MauticPlugin\MauticVoodooBundle\EventListener;

use Mautic\CoreBundle\Helper\ArrayHelper;
use MauticPlugin\MauticVoodooBundle\Model\SmsDeliveryStatModel;
use Mautic\DashboardBundle\Event\WidgetDetailEvent;
use Mautic\DashboardBundle\EventListener\DashboardSubscriber as MainDashboardSubscriber;
use Mautic\SmsBundle\Model\SmsModel;

class DashboardSubscriber extends MainDashboardSubscriber
{
    /**
     * Define the name of the bundle/category of the widget(s).
     *
     * @var string
     */
    protected $bundle = 'sms';

    /**
     * Define the widget(s).
     *
     * @var string
     */
    protected $types = [
        'sms.in.time' => [],
       
    ];

    /**
     * Define permissions to see those widgets.
     *
     * @var array
     */
    protected $permissions = [
        'sms:smses:viewown',
        'sms:smses:viewother',
    ];

    /**
     * @var smsDeliveryStatModel
     */
    protected $smsDeliveryStatModel;


    public function __construct(SmsDeliveryStatModel $smsDeliveryStatModel)
    {
        $this->smsDeliveryStatModel = $smsDeliveryStatModel;
    }

    /**
     * Set a widget detail when needed.
     */
    public function onWidgetDetailGenerate(WidgetDetailEvent $event)
    {
     
        $this->checkPermissions($event);
        
        if ('sms.in.time' == $event->getType()) {
            $widget     = $event->getWidget();
            $params     = $widget->getParams();
            
            if (!$event->isCached()) {

                $data = $this->smsDeliveryStatModel->getSmsLineChartData(
                    $params['timeUnit'],
                    $params['dateFrom'],
                    $params['dateTo'],
                    $params['dateFormat']
                );

                $event->setTemplateData([
                    'chartType'   => 'line',
                    'chartHeight' => $widget->getHeight() - 80,
                    'chartData'   => $data,
                ]);
            }

            $event->setTemplate('MauticCoreBundle:Helper:chart.html.php');
            $event->stopPropagation();
        } 
    }
}
