<?php


namespace MauticPlugin\MauticVoodooBundle\EventListener;

use Mautic\CoreBundle\EventListener\CommonSubscriber;
use Mautic\CoreBundle\Doctrine\Provider\GeneratedColumnsProviderInterface;
use Mautic\ReportBundle\Event\ReportBuilderEvent;
use Mautic\ReportBundle\Event\ReportGeneratorEvent;
use Mautic\ReportBundle\Event\ReportGraphEvent;
use Mautic\ReportBundle\ReportEvents;
use Mautic\CoreBundle\Helper\Chart\ChartQuery;
use Mautic\CoreBundle\Helper\Chart\LineChart;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
/**
 * Class ReportSubscriber
 */
class ReportSubscriber implements EventSubscriberInterface
{
    const CONTEXT_SMS      = 'sms';

        /**
     * @var GeneratedColumnsProviderInterface
     */
    private $generatedColumnsProvider;

    public function __construct(GeneratedColumnsProviderInterface $generatedColumnsProvider) {
        $this->generatedColumnsProvider = $generatedColumnsProvider;
    }

    /**
     * @return array
     */
    static public function getSubscribedEvents()
    {
        return array(
            ReportEvents::REPORT_ON_BUILD          => array('onReportBuilder', 0),
            ReportEvents::REPORT_ON_GENERATE       => array('onReportGenerate', 0),
            ReportEvents::REPORT_ON_GRAPH_GENERATE => array('onReportGraphGenerate', 0)
        );
    }

    /**
     * Add available tables, columns, and graphs to the report builder lookup
     *
     * @param ReportBuilderEvent $event
     *
     * @return void
     */
    public function onReportBuilder(ReportBuilderEvent $event)
    {
        $prefix = 's.';
        // Use checkContext() to determine if the report being requested is this report
        if ($event->checkContext([self::CONTEXT_SMS])) {
            // Define the columns that are available to the report.
            $columns = array(
          
                $prefix.'is_failed' => array(
                    'label' => 'mautic.sms.report.failed',
                    'type'  => 'int'
                ),
                $prefix.'date_sent' => array(
                    'label' => 'mautic.sms.report.date_sent',
                    'type'  => 'datetime'
                ),
                $prefix.'date_delivered' => array(
                    'label' => 'mautic.sms.report.date_delivered',
                    'type'  => 'datetime'
                ),
                $prefix.'reason' => array(
                    'label' => 'mautic.sms.report.reason',
                    'type'  => 'text'
                ),
            );
           

             // Several helper functions are available to append common columns such as categories, publish state fields, lead, etc.  Refer to the ReportBuilderEvent class for more details.
            $columns = array_merge(
                $columns, 
                $event->getStandardColumns($prefix, [], 'mautic_sms_action'), 
                $event->getLeadColumns()
            );
            
            $data = [
                'display_name' => 'mautic.sms.deliverability',
                'columns'      => $columns,
            ];

            // Add the table to the list
            $event->addTable(self::CONTEXT_SMS, $data);

          
            // Register available graphs; can use line, pie, or table
            $event->addGraph(self::CONTEXT_SMS, 'line', 'mautic.sms.graph.line.sent');
        }
    }

    /**
     * Initialize the QueryBuilder object used to generate the report's data.
     * This should use Doctrine's DBAL layer, not the ORM so be sure to use
     * the real schema column names (not the ORM property names) and the
     * MAUTIC_TABLE_PREFIX constant.
     *
     * @param ReportGeneratorEvent $event
     *
     * @return void
     */
    public function onReportGenerate(ReportGeneratorEvent $event)
    {
        $context = $event->getContext();

        if ($context == self::CONTEXT_SMS) {
            $qb = $event->getQueryBuilder();

            $qb->from(MAUTIC_TABLE_PREFIX .'sms_message_delivery_stat', 's');
            $event->addLeadLeftJoin($qb, 's')->applyDateFilters($qb, 'date_sent', 's');
            $event->setQueryBuilder($qb);
        }
    }

    /**
     * Generate the graphs
     *
     * @param ReportGraphEvent $event
     *
     * @return void
     */
    public function onReportGraphGenerate(ReportGraphEvent $event)
    {
        $graphs   = $event->getRequestedGraphs();
        if (!$event->checkContext([self::CONTEXT_SMS])) {
            return;
        }

        $graphs   = $event->getRequestedGraphs();
        $qb       = $event->getQueryBuilder();

        foreach ($graphs as $graph) {
            $options      = $event->getOptions($graph);
            $queryBuilder = clone $qb;
            $chartQuery   = clone $options['chartQuery'];
            $origQuery    = clone $queryBuilder;
            $chartQuery->applyDateFilters($queryBuilder, 'date_sent', 'w');

            switch ($graph) {
                case 'mautic.sms.graph.line.sent':
                   // $chartQuery->setGeneratedColumnProvider($this->generatedColumnsProvider);
                    $chart = new LineChart(null, $options['dateFrom'], $options['dateTo']);
                    $sentQuery = clone $origQuery;
                    $failedQuery = clone $queryBuilder;
                    $failedQuery->andWhere($qb->expr()->eq('s.is_failed', 1));
                    $deliveredQuery = clone $queryBuilder;
                    $deliveredQuery->andWhere($qb->expr()->isNull('s.is_failed'));
                    $chartQuery->modifyTimeDataQuery($sentQuery, 'date_sent', 's');
                    $chartQuery->modifyTimeDataQuery($failedQuery, 'date_sent', 's');
                    $chartQuery->modifyTimeDataQuery($deliveredQuery, 'date_sent', 's');
                    $sends = $chartQuery->loadAndBuildTimeData($sentQuery);
                    $failed = $chartQuery->loadAndBuildTimeData($failedQuery);
                    $delivered = $chartQuery->loadAndBuildTimeData($deliveredQuery);
                    $chart->setDataset($options['translator']->trans('mautic.sms.graph.line.sent'), $sends);
                    $chart->setDataset($options['translator']->trans('mautic.sms.graph.line.failed'), $failed);
                    $chart->setDataset($options['translator']->trans('mautic.sms.graph.line.delivered'), $delivered);

                    $data         = $chart->render();
                    $data['name'] = $graph;
                  
                    $event->setGraph($graph, $data);

                    break;
            }
        }
    }
}