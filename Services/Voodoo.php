<?php

namespace MauticPlugin\MauticVoodooBundle\Services;

use Mautic\LeadBundle\Entity\Lead;
use Mautic\SmsBundle\Api\AbstractSmsApi;
use Monolog\Logger;
use Mautic\PluginBundle\Helper\IntegrationHelper;
use Joomla\Http\Http;
use Mautic\PageBundle\Model\TrackableModel;
use Doctrine\ORM\EntityManagerInterface;
use MauticPlugin\MauticVoodooBundle\Entity\SmsDeliveryStat;
use Doctrine\DBAL\DBALException;

class Voodoo extends AbstractSmsApi
{

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var IntegrationHelper
     */
    private $integrationHelper;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Voodoo Transport constructor.
     */
    public function __construct(TrackableModel $pageTrackableModel, IntegrationHelper $integrationHelper, Logger $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->integrationHelper = $integrationHelper;
        $this->em = $em;

        parent::__construct($pageTrackableModel);
    }

    /**
     * @param string $content
     *
     * @return bool|string
     */
    public function sendSms(Lead $lead, $content)
    {
        $number = $lead->getLeadPhoneNumber();

        if (null === $number) {
            return false;
        }

        $integration = $this->integrationHelper->getIntegrationObject('Voodoo');

        $keys = $integration->getDecryptedApiKeys();

        $senderID = $integration->getIntegrationSettings()->getFeatureSettings()['sending_phone_number'];
    
        $msg = json_encode(
            [
                'to' => $number,
                'from' => $senderID,
                'msg' => $content,
                'external_reference' => $senderID,
                //'sandbox' => true
            ]
        );
        
        $ch = curl_init('https://api.voodoosms.com/sendsms');
        
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: ' . $keys['apikey']
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $msg);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = json_decode(curl_exec($ch));

        $dtHelper = new \Mautic\CoreBundle\Helper\DateTimeHelper('', 'Y-m-d H:i:m');
        $utcString = $dtHelper->toUtcString();

      
        $stat = new SmsDeliveryStat();
        $stat->setTrackingHash($response->messages[0]->id);
        $stat->setLead($lead);
        $stat->setDateSent($utcString);
        $this->em->persist($stat);
        $this->em->flush();
       
    
        curl_close($ch);
    }

    public function deliveryReport() {

        $integration = $this->integrationHelper->getIntegrationObject('Voodoo');

        $keys = $integration->getDecryptedApiKeys();

     
        $records = $this->em->getRepository(SmsDeliveryStat::class)->findAll();

        foreach($records as $record) {

            if($record->trackingHash != null && ($record->reason === null || $record->reason === 'Sent - Awaiting Delivery')) {
                $ch = curl_init('https://api.voodoosms.com/report?message_id='. $record->trackingHash);
    
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Authorization: ' . $keys['apikey']
                ]);
        
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                
                $response = json_decode(curl_exec($ch));
          

                if (in_array($response->report[0]->status,['Not Delivered', 'Expired', 'Rejected' ])) {
                    $record->setIsFailed(true);
                } 

                if ($response->report[0]->delivered_at) {

                    $dtHelper = \DateTime::createFromFormat('U',$response->report[0]->delivered_at);
                    $utcString = $dtHelper->format('Y-m-d H:i:m');
            
                    $record->setDateDelivered($utcString);
                }
            
                $record->setReason($response->report[0]->status);
                
                $this->em->persist($record);
                $this->em->flush();
            }
        }
       
        return $response;


    }

   

}
