<?php
// plugins\HelloWorldBundle\Command\WorldCommand.php

namespace MauticPlugin\MauticVoodooBundle\Command;

use Mautic\CoreBundle\Command\ModeratedCommand;
use MauticPlugin\MauticVoodooBundle\Services\Voodoo;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * CLI Command to send a scheduled broadcast.
 */
class SmsDeliveryReport extends ModeratedCommand
{

    protected $voodoo;
    public function __construct(Voodoo $voodoo) {

        $this->voodoo = $voodoo;

        parent::__construct();
    }
    /** 
     * {@inheritdoc}
     */
    protected function configure()
    {
        // ...

        // Important to append options used by ModeratedCommand
        parent::configure();
        $this->setName('mautic:sms:report')->setDescription('Requests a delivery report for a SMS');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Validate if the command is already has an instance running
        // A third argument can be passed here if this command is running something unique such as an ID
        if (!$this->checkRunStatus($input, $output)) {
            return 0;
        }
  
        // Execute some stuff
        $res = $this->voodoo->deliveryReport();
      
        $this->output->write('Completed Send Report');
        // Complete this execution
        $this->completeRun();

        return 0;
    }
}