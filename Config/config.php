<?php

return [
    'name'        => 'Voodoo SMS',
    'description' => 'Sends the messages to Voodoo SMS',
    'version'     => '1.1.0',
    'author'      => 'Lee Mellon',
    'routes' => [
        'public' => [
            'voodoosms' => [
                'path'       => '/voodoo',
                'controller' => 'MauticVoodooBundle:Voodoo:index',
                'method' => 'POST',
            ],
        ],
        
    ],

    'menu' => [
        'main' => [
            'items' => [
                'mautic.sms.smses' => [
                    'route'  => 'mautic_sms_index',
                    'access' => ['sms:smses:viewown', 'sms:smses:viewother'],
                    'parent' => 'mautic.core.channels',
                    'checks' => [
                        'integration' => [
                            'Voodoo' => [
                                'enabled' => true,
                            ],
                        ],
                    ],
                    'priority' => 70,
                ],
            ],
        ],
    ],
   

    'services' => [
        'events' => [
            'mautic.sms.report.subscriber' => [
                'class' => \MauticPlugin\MauticVoodooBundle\EventListener\ReportSubscriber::class,
                'arguments' => [
                    'mautic.generated.columns.provider',
                ],
            ],
            'mautic.sms.dashboard.subscriber' => [
                'class' => \MauticPlugin\MauticVoodooBundle\EventListener\DashboardSubscriber::class,
                'arguments' => [
                    'mautic.sms.model.sms_delivery_stat',
                ],
            ],
        ],
        'models' => [
            'mautic.sms.model.sms_delivery_stat' => [
                'class' => \MauticPlugin\MauticVoodooBundle\Model\SmsDeliveryStatModel::class,
                'arguments' => [
                    'mautic.generated.columns.provider',
                ],
            ],
        ],
        'other'   => [
            'mautic.sms.transport.voodoo' => [
                'class'     => \MauticPlugin\MauticVoodooBundle\Services\Voodoo::class,
                'arguments' => [
                    'mautic.page.model.trackable',
                    'mautic.helper.integration',
                    'monolog.logger.mautic',
                    'doctrine.orm.entity_manager'
                ],
                'alias' => 'mautic.sms.config.transport.voodoo',
                'tag'   => 'mautic.sms_transport',
                'tagArguments' => [
                    'integrationAlias' => 'Voodoo',
                ],
            ],
        ],
        'command' => [
            'mautic.sms.command.sms_report' => [
                'class'     => \MauticPlugin\MauticVoodooBundle\Command\SmsDeliveryReport::class,
                'arguments' => [
                    'mautic.sms.transport.voodoo'
                ]
            ],
           
        ],
        'integrations' => [
            'mautic.integration.voodoo' => [
                'class' => \MauticPlugin\MauticVoodooBundle\Integration\VoodooIntegration::class,
                'arguments' => [
                    'event_dispatcher',
                    'mautic.helper.cache_storage',
                    'doctrine.orm.entity_manager',
                    'session',
                    'request_stack',
                    'router',
                    'translator',
                    'logger',
                    'mautic.helper.encryption',
                    'mautic.lead.model.lead',
                    'mautic.lead.model.company',
                    'mautic.helper.paths',
                    'mautic.core.model.notification',
                    'mautic.lead.model.field',
                    'mautic.plugin.model.integration_entity',
                    'mautic.lead.model.dnc',
                ],
            ],
        ],
    ]
];